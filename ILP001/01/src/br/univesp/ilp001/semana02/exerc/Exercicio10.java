package br.univesp.ilp001.semana02.exerc;

import java.util.*;

class Exercicio10 {

	public static void main(String[] args) { 
		
		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("\nDigite um numero inteiro com 5 caracteres: ");
		int A = input.nextInt();

		System.out.println("Separacao dos numeros:\n");
		System.out.print(" "
				+ ( A / 10000 )
				+ " " + (A % 10000)/1000 
				+ " " + (A % 1000)/100
				+ " " + (A % 100)/10
				+ " " + (A % 10)
				);
		
		input.close();
	} 

}