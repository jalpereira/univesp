package br.univesp.ilp001.semana02.exerc;

import java.util.*;

class Exercicio09 {

	public static void main(String[] args) { 
		
		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("\nDigite um numero inteiro: ");
		int A = input.nextInt();
		System.out.println("\nDigite outro numero inteiro: ");
		int B = input.nextInt();

		System.out.println("Operacoes Matematicas.\n"
				+ " Soma: " + (A + B)
				+ "\n Produto: " + (A * B)
				+ "\n Diferenca: " + ( A - B)
				+ "\n Quociente: " + ( A / B) 
				);
		input.close();
		
	} 

}