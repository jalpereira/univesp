package br.univesp.ilp001.semana02.exerc;

import java.util.*;

class Exercicio05 {
	
	public static double AreaQ (double lado) { 
		return lado*lado;
	}
	
	public static double AreaQA (double lado) { 
		return Math.pow(lado, 2);
	}
	
	public static double AreaTriReto (double cateto) { 
		return cateto * cateto / 2;
	}
	
	public static double AreaParalelo (double base, double altura ) {
		return base * altura;
	}
	
	public static Integer TipoMat () {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Digite o tipo de Material.\n 1. Metal\n 2. Plastico\n 3. Borracha");
		int tipo = input.nextInt();
		
		input.close();
		
		return tipo;
		
	}

	public static void main(String[] args) { 

		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);

		double quadA = 0.0;
		double triaI = 0.0;
		double triaII = 0.0;
		double paraAlt = 0.0;
		double paraBase = 0.0;
		double BaseB = 0.0;
		
		double cMetal = 0.0;
		double cPlastico = 0.0;
		double cBorracha = 0.0;
		
		int qtdTriA = 0;
		int qtdTriB = 0;
		int qtdQuad = 0;
		int qtdPara = 0;
		
		double TMet = 0.0;
		double TPla = 0.0;
		double TBor = 0.0;
		
		double TQuad ;
		double TTri ;
		double TPara ;
		
		int TTipo; 
		
		// entrada do valor metal e plastico
		System.out.println("Digite o Custo do Metal (2.5): ");
		cMetal = input.nextDouble();
		System.out.println("Digite o Custo do Plastico (1.5): ");
		cPlastico = input.nextDouble();
		System.out.println("Digite o Custo da Borracha (1.0): ");
		cBorracha = input.nextDouble();		

		// QUADRADOS
		// Solicita a Quantidade de Quadrados
		System.out.println("Informe a quantidade de quadrados: ");
		qtdQuad = input.nextInt();
		// solicita entrada de dados para o quadrado
		System.out.println("Calculador de áreas.\n Digite a área (2.5): ");
		quadA = input.nextDouble();
		
		System.out.println("Area Quadrado: " + AreaQ(quadA));
		System.out.println("Area Quadrado Match: " + AreaQA(quadA));
		
		// Verifica o Tipo de Material
		TTipo = TipoMat();
		if ( TTipo == 1 ) {
			TMet = TMet + ( (qtdQuad * AreaQA(quadA)) * cMetal);
		} else if  ( TTipo == 2 ) {
			TPla = TPla + ((qtdQuad * AreaQA(quadA)) * cPlastico);
		} else if ( TTipo == 3 ) {
			TBor = TBor + ((qtdQuad * AreaQA(quadA)) * cBorracha);
		}
		// Registro da area do quadrado
		TQuad = (qtdQuad * AreaQA(quadA));

		// TRIANGULOS		
		// Solicita a Quantidade de Triangulos A
		System.out.println("Informe a quantidade de triangulos 1: ");
		qtdTriA = input.nextInt();
		// solicita entrada de dados para o triangulo
		System.out.println("Calculador de áreas.\n Digite tamanho do cateto (4): ");
		triaI = input.nextDouble();
		
		System.out.println("Area Triangulo 1: " + AreaTriReto(triaI));
		
		// Verifica o Tipo de Material
		TTipo = TipoMat();
		if ( TTipo == 1 ) {
			TMet =  TMet + (( qtdTriA * AreaTriReto(triaI) ) * cMetal);
		} else if  ( TTipo == 2 ) {
			TPla = TMet + ((qtdQuad * AreaQA(quadA)) * cPlastico);
		} else if ( TTipo == 3 ) {
			TBor = TBor + ((qtdQuad * AreaQA(quadA)) * cBorracha);
		}
		
		// Solicita a Quantidade de Triangulos B
		System.out.println("Informe a quantidade de triangulos 2: ");
		qtdTriB = input.nextInt();
		// solicita entrada de dados para o triangulo
		System.out.println("Calculador de áreas.\n Digite tamanho do cateto (4): ");
		triaII = input.nextDouble();
		
		System.out.println("Area Triangulo 1: " + AreaTriReto(triaII));
		
		// Verifica o Tipo de Material
		TTipo = TipoMat();
		if ( TTipo == 1 ) {
			TMet =  TMet + (( qtdTriB * AreaTriReto(triaII) ) * cMetal );
		} else if  ( TTipo == 2 ) {
			TPla = TPla + (( qtdTriB * AreaTriReto(triaII) ) * cPlastico );
		} else if ( TTipo == 3 ) {
			TBor = TBor + (( qtdTriB * AreaTriReto(triaII) ) * cBorracha );
		}
		
		TTri = (( qtdTriA * AreaTriReto(triaI) ) + ( qtdTriB * AreaTriReto(triaII)) ) ;
		
		// PARALELOGRAMO
		// Solicita a Quantidade de Paralelogramo
		System.out.println("Informe a quantidade de paralelogramo: ");
		qtdPara = input.nextInt();		
		
		// solicita a entrada de dados para o Paralelogramo
		System.out.println("Calculador de áreas.\n Digite base do paralelogramo (4): ");
		paraBase = input.nextDouble();
		System.out.println("Calculador de áreas.\n Digite a Altura do paralelogramo (2.5): ");
		paraAlt = input.nextDouble();
		
		System.out.println("Area Paralelogramo : " + AreaParalelo(paraBase, paraAlt));

		// Verifica o Tipo de Material
		TTipo = TipoMat();
		if ( TTipo == 1 ) {
			TMet =  TMet + (( qtdPara * AreaParalelo(paraBase, paraAlt) ) * cMetal );
		} else if  ( TTipo == 2 ) {
			TPla = TPla + (( qtdPara * AreaParalelo(paraBase, paraAlt) ) * cPlastico );
		} else if ( TTipo == 3 ) {
			TBor = TBor + (( qtdPara * AreaParalelo(paraBase, paraAlt) ) * cBorracha );
		}		
		
		TPara = ( qtdPara * AreaParalelo(paraBase, paraAlt) ) ;
		
		// Area de Base
		// Solicita o Tamanho da Base
		System.out.println("Calculador de áreas.\n Digite o tamanho da Base: ");
		BaseB = input.nextDouble();		
		
		// Verifica o Tipo de Material
		TTipo = TipoMat();
		if ( TTipo == 1 ) {
			TMet =  TMet + (( Math.pow(BaseB, 2) ) * cMetal );
		} else if  ( TTipo == 2 ) {
			TPla = TPla + (( Math.pow(BaseB, 2) ) * cPlastico );
		} else if ( TTipo == 3 ) {
			TBor = TBor + (( Math.pow(BaseB, 2) ) * cBorracha );
		}
		
		input.close();
		
		System.out.println("Custo total de Metal: " + TMet
				+ "\nCusto total de Plastico: " + TPla
				+ "\nCusto total de Borracha: " + TBor
				);
	} 

}