package br.univesp.ilp001.semana02.exerc;

import java.util.*;

class Exercicio06 {
	
	public static float Convert (float real, float rate) { 
		return real / rate;
	}

	public static void main(String[] args) { 
		
		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		float rate = 0;
		float real = 0;
		
		System.out.println("Digite a taxa de cotacao do dolar: \n");
		rate = input.nextFloat();

		System.out.println("Digite o valor em Reais R$: \n");
		real = input.nextFloat();
		
		System.out.println("A conversão de R$ " + real + " para dolar é de US$ " + (Convert(real,rate) ));
		input.close();
	} 

}