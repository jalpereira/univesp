package br.univesp.ilp001.semana02.exerc;

class Exercicio03 {
	
	public static Integer CalcA(int a, int x){		
		int y = a * x * x * x + 7;
		return y;
	}
	public static Integer CalcB(int a, int x){
		int y = a * x * x * (x + 7);
		return y;
	}
	public static Integer CalcC(int a, int x){
		int y = (a * x) * x * (x + 7);
		return y;
	}
	public static Integer CalcD(int a, int x){
		int y = (a * x) * x * x + 7;
		return y;
	}
	public static Integer CalcE(int a, int x){
		int y = a * (x * x * x) + 7;
		return y;
	}
	public static Integer CalcF(int a, int x){
		int y = a * x * (x * x + 7);
		return y;
	}
	
	public static void main(String[] args) {
		int a = 1;
		int x = 3;
		
		double cub = Math.pow( x , 3);
		int RES = a * ((int) cub) + 7;
		System.out.println(RES);
		
		int A = CalcA(a,x);
		int B = CalcB(a,x);
		int C = CalcC(a,x);
		int D = CalcD(a,x);
		int E = CalcE(a,x);
		int F = CalcF(a,x);
		
		
		System.out.println((A==RES) ? "Res. A: " + A +" [correta]" : "Res. A: " + A + "[incorreta]" );
		System.out.println((B==RES) ? "Res. B: " + B +" [correta]" : "Res. B: " + B + "[incorreta]" );
		System.out.println((C==RES) ? "Res. C: " + C +" [correta]" : "Res. C: " + C + "[incorreta]" );
		System.out.println((D==RES) ? "Res. D: " + D +" [correta]" : "Res. D: " + D + "[incorreta]" );
		System.out.println((E==RES) ? "Res. E: " + E +" [correta]" : "Res. E: " + E + "[incorreta]" );
		System.out.println((F==RES) ? "Res. F: " + F +" [correta]" : "Res. F: " + F + "[incorreta]" );

		// Res. A: 23 [correta]
		// Res. B: 72[incorreta]
		// Res. C: 72[incorreta]
		// Res. D: 23 [correta]
		// Res. E: 23 [correta]
		// Res. F: 44[incorreta]
		
	}	
}