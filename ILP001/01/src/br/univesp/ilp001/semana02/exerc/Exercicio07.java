package br.univesp.ilp001.semana02.exerc;

import java.util.*;

class Exercicio07 {
	
	public static double Queda(double Altura) { 
		double gravi = 9.8;
		
		return Math.sqrt(2 * Altura / gravi );
	}

	public static void main(String[] args) { 
		double result;
		
		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("Digite a altura da queda: \n");
		double altura = (input.nextDouble()/100);

		result = Queda(altura);
		
		System.out.println("O tempo de queda de uma altura de " + altura + "mt. é de: " + result + "s\n");
		input.close();
	} 

}