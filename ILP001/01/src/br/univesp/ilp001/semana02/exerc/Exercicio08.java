package br.univesp.ilp001.semana02.exerc;

import java.util.*;

class Exercicio08 {
	
	public static float Multa(int Qtd, String Tipo) {
		double valor = 0.0;
		double vAmar = 1000.20;
		double vVerm = 4523.75;
		
		if (Tipo == "A" ) {
			valor =  (double)Qtd * vAmar;
		} else if (Tipo == "V") {
			valor = (double)Qtd * vVerm;
		} 
		
		return (float)valor;
		
	}

	public static void main(String[] args) { 
		float result = (float)0;
		int Time;
		int Jogador;
		int Amarelos;
		int Vermelhos;
		
		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("Digite o numero do Time: \n");
		Time = input.nextInt();
		System.out.println("Digite o numero do jogador: \n");
		Jogador = input.nextInt();
		System.out.println("Digite o numero de Cartões Amarelos: \n");
		Amarelos = input.nextInt();
		System.out.println("Digite o numero de Cartões Vermelhos: \n");
		Vermelhos = input.nextInt();		

		result = result + Multa(Amarelos, "A");
		result = result + Multa(Vermelhos, "V");

		input.close();
		
		System.out.println("O time : " + Time + " e o Jogador: " + Jogador
				+ " deverão pagar R$ " + result + " de Multa");
		
	} 

}