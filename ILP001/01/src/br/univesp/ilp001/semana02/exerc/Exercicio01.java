package br.univesp.ilp001.semana02.exerc;

class Exercicio01 {

	public static String Calc1() { 
		// variaveis		
		int a = 3;
		int b = 5;
		int c = 8;
		int d = a * (b + c * 3) - 7;
		int e = a - b - c;
		
		return(a+" "+b+" "+c+" "+d+" "+e);
	}

	public static String Calc2() { 
		// variaveis
		int a = 3;
		int b = 5;
		int c = 8;
		int d = a * (b + c * 3) - 7;
		int e = a - b - c;
		
		a = a + 1;
		b = (4 * a + 1) / 10;
		c = (4 * a + 1) % 10;

		return(a+" "+b+" "+c+" "+d+" "+e);
	}
	
	public static void main(String[] args) { 
		String calcA;
		String calcB;
		
		calcA = Calc1();
		
		System.out.println("Resultado 1: " +calcA); 
		// resp. A - 3 5 8 80 -10

		calcB = Calc2();		
		
		System.out.println("Resultado 1: " +calcB); 
		// resp. B - 4 1 7 80 -10
	}	
}