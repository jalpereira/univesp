package br.univesp.ilp001.semana02.exerc;

import java.util.*;

class Exercicio02 {
	public static String respA (){ 
		// p = i + j + k + 7;
		int p;
		int i;
		int j;
		int k;
		
		i = 0;
		k = 0;
		j = 0;
		
		p = i + j + k + 7;
		
		return("Resultado A: " +p); 

	}
	public static String respC() {
		return("a = 5");
	}
	
	public static Integer respD() {

		Scanner input = new Scanner(System.in);
	
		System.out.println("digite numero inteiro: "); 
		int value = input.nextInt();
		
		input.close();
		
		return value;
	}	
	
	public static void main(String[] args) { 
		System.out.println(respA());
		//resp. A  -  k, i, j sao variaveis
		System.out.println("variables whose values are modified"); 
		//resp. B  -  �variables whose values are modified� nao � variavel
		System.out.println(respC());
		// resp. B  -  �a = 5� nao � variavel
		System.out.println(respD());
		// resp. D  -  �value� � variavel
		System.out.println("Fim da Classe");
	}	
}