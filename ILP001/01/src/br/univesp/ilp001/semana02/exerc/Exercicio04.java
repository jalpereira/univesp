package br.univesp.ilp001.semana02.exerc;

class Exercicio04 {
	
	public static void main(String[] args) {
		
		System.out.println("7 + 3 * 6 / 2 - 1 --> primeiro * e / . esq para direira\n"
				+ "7 + 3 * 6 / 2 – 1 --> 7 + 18/2 – 1 "
				+ "--> 7 + 9 – 1 --> 16 – 1 "
				+ "--> 15"); 
		// resp. A
		
		System.out.println("2 % 2 + 2 * 2 - 2 / 2 --> primeiro *, / e % . esq para direita\n"
				+ "2 % 2 + 2 * 2 - 2 / 2 → 0 + 2 * 2 - 2 / 2 "
				+ "→ 0 + 4 - 2 / 2 → 0 + 4 - 2 / 2 → 0 + 4 – 1 → 4 – 1 → 3");
		// resp. B
		
		System.out.println("(3 * 9 * (3 + (9 * 3 / (3)))) --> primeiro parenteses, depois * e /, esq para direita\n"
				+ "(3 * 9 * (3 + (9 * 3 / (3)))) → (3 * 9 * (3 + (9 * 3 / 3))) → (3 * 9 * (3 + (27 / 3))) "
				+ "→ (3 * 9 * (3 + (9))) → (3 * 9 * (3 + 9)) → (3 * 9 * (12)) → (3 * 9 * 12) "
				+ "→ (3 * 108) → 324");
		// resp. C		
	}	
}