package br.univesp.ilp001.semana03.teste;

import java.util.Locale;
import java.util.Scanner;

public class Exercicio01 {

	static void checkTrianguloA(double l1, double l2, double l3) {
		if(l1 == l2 || l1 == l3 || l2 == l3){
			System.out.println("Equilatero");
		}
		if(l1 == l2 && l1 == l3 && l2 != l3){
			System.out.println("Isosceles nao equilatero");
		}
		if(l1 != l2 && l1 != l3 && l2 != l3){
			System.out.println("Escaleno");
		}		
	}
	
	static void checkTriangulo2(double l1, double l2, double l3){
/*		if(l1 == l2 == l3){
				System.out.println("Equilatero");
		} else {
			if(l1 == l2 != l3){
				System.out.println("Isosceles nao equilatero");
			} else {
				System.out.println("Escaleno");
			}
		} */
		// ERRO NA STRING
 	}

	static void checkTriangulo3(double l1, double l2, double l3){
		   if(l1 == l2 && l1 == l3 && l2 == l3){
		      System.out.println("Equilatero");
		   }
		   if(l1 == l2 || l1 == l3 || l2 != l3){
		      System.out.println("Isosceles nao equilatero");
		   }
		   if(l1 != l2 || l1 != l3 || l2 != l3){
		      System.out.println("Escaleno");
		   }
		}

	static void checkTriangulo4(double l1, double l2, double l3){
		   if(l1 == l2 && l1 == l3){
		      System.out.println("Equilatero");
		   } else {
		      if(l1 != l2 || l1 != l3){
		         System.out.println("Isosceles nao equilatero");
		      } else {
		         System.out.println("Escaleno");
		      }
		   }
		}
	
	static void checkTriangulo5(double l1, double l2, double l3){
		   if(l1 == l2 && l1 == l3){
		      System.out.println("Equilatero");
		   } else {
		      if(l1 == l2 || l1 == l3 || l2 == l3){
		         System.out.println("Isosceles nao equilatero");
		      } else {
		         System.out.println("Escaleno");
		      }
		   }
		}
	
	// Executor
	public static void main(String[] args) {
		
		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("\nDigite tr�s numeros (separados por espa�o): ");
		double n1 = input.nextDouble();
		double n2 = input.nextDouble();
		double n3 = input.nextDouble();
		
		// Chamada resposta A
		System.out.println("Resposta A: ");
		checkTrianguloA(n1, n2, n3);

		// Chamada resposta B
		System.out.println("Resposta B: ");
		checkTriangulo2(n1, n2, n3);
		
		// Chamada resposta C
		System.out.println("Resposta C: ");
		checkTriangulo3(n1, n2, n3);

		// Chamada resposta D
		System.out.println("Resposta D: ");
		checkTriangulo4(n1, n2, n3);					
		// Chamada resposta E
		System.out.println("Resposta E: ");
		checkTriangulo5(n1, n2, n3);
			
		input.close();
	}
}
