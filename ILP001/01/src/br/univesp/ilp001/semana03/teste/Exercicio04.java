package br.univesp.ilp001.semana03.teste;

import java.util.Locale;
import java.util.Scanner;

public class Exercicio04 {

	   //Poss�veis resultados
	   final static int APROVADO_DIRETO = 0; //Aprovado na P1,P2,P3
	   final static int APROVADO_2AVALIACAO = 1; //Aprovado ap�s PREC
	   final static int REPROVADO_FREQUENCIA = 2; //Reprovado por frequ�ncia
	   final static int REPROVADO_DIRETO = 3; //N�o autorizado a fazer REC
	   final static int REPROVADO_2AVALIACAO = 4; //Reprovado ap�s fazer REC
	 
	   static int avaliar (double P1, double P2, double P3, double PREC, int freq){
		      double mediaProvas = (P1+P2+P3)/3;
		      double mediaRec = (mediaProvas + PREC)/2;   

		      if(freq < 70){
		    	   return REPROVADO_FREQUENCIA;
		    	}
		    	  
		    	if(mediaProvas >= 5.0){
		    	   return APROVADO_DIRETO;
		    	} else {
		    	   if(mediaProvas < 3.0){
		    	      return REPROVADO_DIRETO;
		    	   } else {
		    	      if (mediaRec >= 5.0){
		    	         return APROVADO_2AVALIACAO;
		    	      } else {
		    	         return REPROVADO_2AVALIACAO;
		    	      }
		    	   }
		    	}
	   }
	
	// Executor
	public static void main(String[] args) {
		int resultado;

		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("\nInsira 4 notas e a frequencia (separados por espa�o): ");
		Double n1 = input.nextDouble();
		Double n2 = input.nextDouble();	
		Double n3 = input.nextDouble();	
		Double nr = input.nextDouble();	
		int fr = input.nextInt();		
		
		resultado = avaliar(n1, n2, n3, nr, fr);	
		System.out.println("\nResposta 3A: " + resultado);

		switch(resultado){
		   case APROVADO_DIRETO:
		      System.out.println("Aprovado direto.");
		      break;
		   case APROVADO_2AVALIACAO:
		      System.out.println("Aprovado na 2a avaliacao.");
		      break;
		   case REPROVADO_FREQUENCIA:
		      System.out.println("Reprovado por frequencia.");
		      break;
		   case REPROVADO_DIRETO:
		      System.out.println("Reprovado sem direito a recuperacao.");
		      break;
		   case REPROVADO_2AVALIACAO:
		      System.out.println("Reprovado apos a recuperacao.");
		      break;
		}
		
	}
}
