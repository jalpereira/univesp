package br.univesp.ilp001.semana03.teste;

import java.util.Locale;
import java.util.Scanner;

public class Exercicio03 {

	   //Poss�veis resultados
	   final static int APROVADO_DIRETO = 0; //Aprovado na P1,P2,P3
	   final static int APROVADO_2AVALIACAO = 1; //Aprovado ap�s PREC
	   final static int REPROVADO_FREQUENCIA = 2; //Reprovado por frequ�ncia
	   final static int REPROVADO_DIRETO = 3; //N�o autorizado a fazer REC
	   final static int REPROVADO_2AVALIACAO = 4; //Reprovado ap�s fazer REC
	 
	   static int avaliar1 (double P1, double P2, double P3, double PREC, int freq){
		      double mediaProvas = (P1+P2+P3)/3;
		      double mediaRec = (mediaProvas + PREC)/2;   

		      if(freq < 70){
		    	   return REPROVADO_FREQUENCIA;
		    	}
		    	  
		    	if(mediaProvas >= 5.0){
		    	   return APROVADO_DIRETO;
		    	} else {
		    	   if(mediaProvas < 3.0){
		    	      return REPROVADO_DIRETO;
		    	   } else {
		    	      if (mediaRec >= 5.0){
		    	         return APROVADO_2AVALIACAO;
		    	      } else {
		    	         return REPROVADO_2AVALIACAO;
		    	      }
		    	   }
		    	}
	   }
	   
	   static int avaliar2 (double P1, double P2, double P3, double PREC, int freq){
		      double mediaProvas = (P1+P2+P3)/3;
		      double mediaRec = (mediaProvas + PREC)/2;   

		      if(mediaProvas >= 5.0 || freq >= 70){
		    	  return APROVADO_DIRETO;
		      } else {
		      
		      if (mediaRec >= 5.0){
		    	  return APROVADO_2AVALIACAO;
		      } else {
		    	  return REPROVADO_2AVALIACAO;
		      }
		    		
	/* CORRIGIDO
	 * 	      if(mediaProvas < 3.0 && mediaRec < 5.0){
		    	  return REPROVADO_DIRETO;
		      } else {
		    	  return REPROVADO_2AVALIACAO;
		      } 
		    	   
		      if (freq < 70){
		    	  return REPROVADO_FREQUENCIA;
		      } */
		      }
		      
		   }
	   static int avaliar3 (double P1, double P2, double P3, double PREC, int freq){
		      double mediaProvas = (P1+P2+P3)/3;
		      double mediaRec = (mediaProvas + PREC)/2;   

		      if(freq < 70){
		    	   return REPROVADO_FREQUENCIA;
		    	}
		    	  
		    	if(mediaProvas >= 5.0 && mediaRec >= 5.0){
		    	   return APROVADO_DIRETO;
		    	} else {
		    	   if(mediaProvas < 3.0 && mediaRec < 5.0){
		    	      return REPROVADO_DIRETO;
		    	   } else {
		    	      return APROVADO_2AVALIACAO;
		    	   }
		    	}
		    	// return REPROVADO_2AVALIACAO; CORRIGIDO
		      
		   }
	   static int avaliar4 (double P1, double P2, double P3, double PREC, int freq){
		      double mediaProvas = (P1+P2+P3)/3;
		      double mediaRec = (mediaProvas + PREC)/2;   

		      if(mediaProvas >= 5.0 && freq > 70){
		    	   return APROVADO_DIRETO;
		    	} else {
		    	   if(mediaRec <= 3.0){
		    	      return REPROVADO_DIRETO;
		    	   } else {
		    	      if (mediaRec < 5.0){
		    	         return APROVADO_2AVALIACAO;
		    	      } else {
		    	         return REPROVADO_2AVALIACAO;
		    	      }
		    	   }
		    	}
		      
		   }
	   static int avaliar5 (double P1, double P2, double P3, double PREC, int freq){
		      double mediaProvas = (P1+P2+P3)/3;
		      double mediaRec = (mediaProvas + PREC)/2;   

		      if(mediaProvas >= 5.0){
		    	   return APROVADO_DIRETO;
		    	} else {
		    	   if (mediaRec >= 5.0){
		    	      return APROVADO_2AVALIACAO;
		    	   } else {
		    	      return REPROVADO_2AVALIACAO;
		    	   }
		    	   /* CORRIGIDO
		    	   if(mediaProvas < 3.0){
		    	    
		    	      return REPROVADO_DIRETO;
		    	   } else {
		    	      return APROVADO_2AVALIACAO;
		    	} 
		    	 
		    	if(freq < 70){
		    	   return REPROVADO_FREQUENCIA;
		    	} */
		    	}
		   }	   
	
	// Executor
	public static void main(String[] args) {
		// APROVADO_DIRETO = 0; //Aprovado na P1,P2,P3
		// APROVADO_2AVALIACAO = 1; //Aprovado ap�s PREC
		// REPROVADO_FREQUENCIA = 2; //Reprovado por frequ�ncia
		// REPROVADO_DIRETO = 3; //N�o autorizado a fazer REC
		// REPROVADO_2AVALIACAO = 4; //Reprovado ap�s fazer REC		
		
		int resultado;

		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("\nInsira 4 notas e a frequencia (separados por espa�o): ");
		Double n1 = input.nextDouble();
		Double n2 = input.nextDouble();	
		Double n3 = input.nextDouble();	
		Double nr = input.nextDouble();	
		int fr = input.nextInt();	
		
		resultado = avaliar1(n1, n2, n3, nr, fr);
		System.out.println("\nResposta 3A: " + resultado);

		resultado = avaliar2(4.0, 5.0, 6.0, 0.0, 70);
		System.out.println("\nResposta 3A: " + resultado);
		
		resultado = avaliar3(4.0, 5.0, 6.0, 0.0, 70);
		System.out.println("\nResposta 3A: " + resultado);
		
		resultado = avaliar4(4.0, 5.0, 6.0, 0.0, 70);
		System.out.println("\nResposta 3A: " + resultado);
		
		resultado = avaliar5(4.0, 5.0, 6.0, 0.0, 70);
		System.out.println("\nResposta 3A: " + resultado);
		
		input.close();
		
	}
}
