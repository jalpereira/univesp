package br.univesp.ilp001.semana03.teste;

public class Exercicio05 {
	 
	static void tabuada(int n){
		   int i = 1;
		   while (i <= 10){
		      System.out.println(n + " x " + i + " = \t" + n*i);
		      i = i + 1;
		   }
		}
	   

	public static void main(String[] args) {

		tabuada(10);
		
	}
}
