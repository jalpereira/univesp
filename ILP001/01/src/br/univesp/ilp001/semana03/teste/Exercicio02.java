package br.univesp.ilp001.semana03.teste;

import java.util.Locale;
import java.util.Scanner;

public class Exercicio02 {

	static void inversor1(int val){
		   while(val > 0){
		      System.out.print(val/10);
		      val = val%10;
		      
		      val = 0; // forcar parada do loop eterno
		   }
		}
	
	static void inversor2(int val){
		   while(val > 0 ){
		      System.out.print(val/10);
		      val = val/10;
		   }
		}

	static void inversor3(int val){
		   while(val > 0 ){
		      System.out.print(val);
		      val = val - 10;
		   }
		}

	static void inversor4(int val){
		   while(val > 0 ){
		      System.out.print(val%10);
		      val = val - 10;
		   }
		}

	static void inversor5(int val){

		   while(val > 0 ){
		      System.out.print(val%10);
		      val = val/10;
		   }
		}
	
	// Executor
	public static void main(String[] args) {
		
		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("\nDigite tr�s numeros (separados por espa�o): ");
		int n1 = input.nextInt();
		
		// Chamada resposta A
		System.out.println("\nResposta A: ");
		inversor1(n1);

		// Chamada resposta B
		System.out.println("\nResposta B: ");
		inversor2(n1);
		
		// Chamada resposta C
		System.out.println("\nResposta C: ");
		inversor3(n1);

		// Chamada resposta D
		System.out.println("\nResposta D: ");
		inversor4(n1);
		
		// Chamada resposta E
		System.out.println("\nResposta E: ");
		inversor5(n1);
			
		input.close();
	}
}
