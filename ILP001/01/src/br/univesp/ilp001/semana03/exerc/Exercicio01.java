package br.univesp.ilp001.semana03.exerc;

class Exercicio01 {

	static void multiplos2357(int num){
		System.out.print("Multiplos de " + num + ": (");
		
		if(num%2 == 0){
			System.out.print(" 2 ");
	    }
		
	    if(num%3 == 0){
	    	System.out.print(" 3 ");
	    }
	    
	    if(num%5 == 0){
	        System.out.print(" 5 ");
	    }

	    if(num%7 == 0){
	        System.out.print(" 7 ");
	    }
	    
	    System.out.println(")");   
	}
	
	public static void main(String[] args) { 

		Integer[] ArreyNum = { 10, 15, 21, 25, 30};
		
		for (int i : ArreyNum ) {
			multiplos2357(i);
		}
		// Multiplos de 10: ( 2  5 )
		// Multiplos de 15: ( 3  5 )
		// Multiplos de 21: ( 3  7 )
		// Multiplos de 25: ( 5 )
		// Multiplos de 30: ( 2  3  5 )
	}
}
