package br.univesp.ilp001.semana03.exerc;

import java.util.Locale;
import java.util.Scanner;

class Exercicio03 {	
	public static void main(String[] args) { 
		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("\nDigite dois numeros inteiros (separados por espa�o): ");
		int n1 = input.nextInt();
		int n2 = input.nextInt();
		
		System.out.print("O numero " + n1 + " � ");
		if ( n1 < n2 ) {
			System.out.print("menor");
		} else if ( n1 == n2 ) {
			System.out.print("igual");
		} else {
			System.out.print("maior");
		}
		System.out.print(" que o numero " + n2 );
		input.close();
	}
}

