package br.univesp.ilp001.semana03.exerc;

import java.util.Locale;
import java.util.Scanner;

class Exercicio02 {	
	static double modulo(double n) {
		if ( n < 0 ) {
			n = n * (-1);
		}
		return n;
	}
	
	public static void main(String[] args) { 
		// define metodo de entrada manual
		Scanner input = new Scanner(System.in);
		input.useLocale(Locale.ENGLISH);
		
		System.out.println("\nDigite um numero: ");
		double n = input.nextDouble();
		
		System.out.println("\nO modulo do numero " + n + " � " + modulo(n) );
		input.close();
	}
}

